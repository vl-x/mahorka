<?php 
	
	/**
	 *
	 * Template footer
	 *
	 **/
	
	// create an access to the template main object
	global $gk_tpl;
	
	// disable direct access to the file	
	defined('GAVERN_WP') or die('Access denied');
	
?>
</div><!-- end of the gk-bg div -->
	<footer id="gk-footer">
		<div class="gk-page">			
			<?php gavern_menu('footermenu', 'gk-footer-menu'); ?>
			
			<div class="gk-copyrights">
				<?php echo str_replace('\\', '', htmlspecialchars_decode(get_option($gk_tpl->name . '_template_footer_content', ''))); ?>
			</div>
			
			<?php if(get_option($gk_tpl->name . '_styleswitcher_state', 'Y') == 'Y') : ?>
			<div id="gk-style-area">
				<?php for($i = 0; $i < count($gk_tpl->styles); $i++) : ?>
				<div class="gk-style-switcher-<?php echo $gk_tpl->styles[$i]; ?>">
					<?php 
						$j = 1;
						foreach($gk_tpl->style_colors[$gk_tpl->styles[$i]] as $stylename => $link) : 
					?> 
					<a href="#<?php echo $link; ?>" id="gk-color<?php echo $j++; ?>"><?php echo $stylename; ?></a>
					<?php endforeach; ?>
				</div>
				<?php endfor; ?>
			</div>
			<?php endif; ?>
			
			<?php if(get_option($gk_tpl->name . '_template_footer_logo', 'Y') == 'Y') : ?>
			<img src="<?php echo gavern_file_uri('images/gavernwp.png'); ?>" class="gk-framework-logo" alt="GavernWP" />
			<?php endif; ?>	
			<!-- Yandex.Metrika informer -->
			<a href="https://metrika.yandex.ru/stat/?id=47556208&amp;from=informer"
			target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/47556208/3_0_FFFFFFFF_EFEFEFFF_0_pageviews"
			style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="47556208" data-lang="ru" /></a>
			<!-- /Yandex.Metrika informer -->
			<!-- Yandex.Metrika counter -->
			<script type="text/javascript" >
				(function (d, w, c) {
					(w[c] = w[c] || []).push(function() {
						try {
							w.yaCounter47556208 = new Ya.Metrika({
								id:47556208,
								clickmap:true,
								trackLinks:true,
								accurateTrackBounce:true
							});
						} catch(e) { }
					});

					var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
					s.type = "text/javascript";
					s.async = true;
					s.src = "https://mc.yandex.ru/metrika/watch.js";

					if (w.opera == "[object Opera]") {
						d.addEventListener("DOMContentLoaded", f, false);
					} else { f(); }
				})(document, window, "yandex_metrika_callbacks");
			</script>
			<noscript><div><img src="https://mc.yandex.ru/watch/47556208" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
			<!-- /Yandex.Metrika counter -->
		</div>
	</footer>
	
	<?php gk_load('popups'); ?>
	<?php gk_load('social'); ?>
	
	<?php do_action('gavernwp_footer'); ?>
	
	<?php 
		echo stripslashes(
			htmlspecialchars_decode(
				str_replace( '&#039;', "'", get_option($gk_tpl->name . '_footer_code', ''))
			)
		); 
	?>
	
	<?php wp_footer(); ?>
	
	<?php do_action('gavernwp_ga_code'); ?>
	
	<script type="text/javascript">
		if(jQuery(window).width() > 600) {
			jQuery('.parallax .box-title').attr('data-scroll-reveal', 'enter bottom and move 100px over 0.75s').addClass('scroll-revealed');
			
			jQuery(window).scrollReveal = new scrollReveal();
		}
	</script>
</body>
</html>
