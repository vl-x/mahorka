<?php
define(TSP27_IM, false); // если true то используется как интернет магазин









































remove_action('wp_head', 'wp_resource_hints', 2); //remove <link rel='dns-prefetch' href='//netdna.bootstrapcdn.com' /> and <link rel='dns-prefetch' href='//s.w.org' />
remove_action('wp_head', 'feed_links', 2); // remove <link rel="alternate" type="application/rss+xml" title="Лента" href="http://site.com/feed/" /> and <link rel="alternate" type="application/rss+xml" title="Лента комментариев" href="http://site.com/comments/feed/" />
remove_action('wp_head', 'feed_links_extra', 3); // remove <link rel="alternate" type="application/rss+xml" title="Лента рубрики" href="http://site.com/category/feed/" /> and similar

remove_action('wp_head', 'rest_output_link_wp_head'); // remove <link rel='https://api.w.org/' href='http://site.com/wp-json/' /> 
remove_action('wp_head', 'rsd_link'); // remove <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://site.com/xmlrpc.php?rsd" />
remove_action('wp_head', 'wlwmanifest_link'); // remove <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://site.com/wp-includes/wlwmanifest.xml" />
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head'); // remove <link rel='prev' title='post1' href='http://site.com/post1/' /> and <link rel='next' title='post2' href='http://site.com/post2/' /> not at all page
remove_action('wp_head', 'wp_shortlink_wp_head',10,0); // remove <link rel='shortlink' href='ttp://site.com/' />
remove_action('wp_head', 'wp_oembed_add_discovery_links'); // remove <link rel="alternate" type="application/json+oembed" href="http://site.com/wp-json/oembed/1.0/embed?url=..." /> and <link rel="alternate" type="text/xml+oembed" href="http://site.com/wp-json/oembed/1.0/embed?url=...;format=xml" />
?>