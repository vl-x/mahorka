��    3      �      L      L     M     e  "   v     �     �     �     �     �     �     �     �               +     3     9  	   >  	   H     R     Y     f     k     s     x     ~     �     �     �     �     �     �     �     �     �  P   �     F     N     W  E   `     �     �     �     �     �     �     �     �  	   �  (         )  �  1  "   
     )
  O   D
     �
     �
     �
     �
     �
  9   	     C     _     l     u     �     �     �     �     �     �     �          )     7  
   @  
   K  
   V     a  %   t  #   �     �  !   �  7         8     G  �   g          -     M  �   d  <   �     3  &   S  %   z     �     �  
   �     �     �  B   �     3   %1$s &rsaquo; Job: %2$s %s &rsaquo; Jobs %s &rsaquo; Manage Backup Archives Abort Add new Add new job Archive Format Archive name Backup File Creation Backup sent via email Backup to Dropbox Backup to FTP Backup to Folder Backups Close Copy DB Backup Dashboard Delete Destinations Edit Errors: File Files Folder General Inactive Job "%s" started. Job Destination Job Name Job completed Job done in %s seconds. Jobs Last Run Leave empty to not have log sent. Or separate with , for more than one receiver. New Job Next Run No Jobs. No files could be found. (List will be generated during next backup.) Please name this job. Run now Save Changes Save changes Settings Size Time Type Warnings: Where should your backup file be stored? not yet Project-Id-Version: BackWPup Pro
Report-Msgid-Bugs-To: 
POT-Creation-Date: Tue Aug 25 2015 08:53:55 GMT+0200 (Mitteleuropäische Sommerzeit)
PO-Revision-Date: Sun Nov 20 2016 19:03:03 GMT+0300
Last-Translator: tsp27 <tsp27@mail.ru>
Language-Team: 
Language: Russian
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Generator: Loco - https://localise.biz/
X-Loco-Target-Locale: ru_RU %1$s &rsaquo; Задание: %2$s %s &rsaquo; Задания %s &rsaquo; Управление архивами резервных копий Прервать Добавить новое Новое задание Формат архива Имя архива Создание файла резервной копии Высылать по email На Dropbox По FTP В папку Резервные копии Закрыть Копировать База данных Консоль Удалить Назначения Редактировать Ошибок: Файл Файлы Папка Общее неактивно Задание "%s" запущено. Назначение задания Название задания Задание завершено Задание выполнено за %s секунд. Задания Последний запуск Оставьте пустым что бы логи не отсылались. Или перечислите через , если несколько получателей Новое задание Следующий запуск Нет заданий. Файлов не найдено. (Список будет обновлён при следующем резервном копировании.) Пожалуйста назовите это задание. Запустить сейчас Сохранить изменения. Сохранить изменения Настройки Размер Время Тип Предупреждений: Где сохранять файл резервной копии ? ещё нету 